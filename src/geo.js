let positions = [{
    lat: 32.109333,
    long: 34.855499
}];


let getPositions = () => {
    return positions;
}

let addPosition = (position) => {
    positions.push({'lat': position.lat, 'long': position.long});
}

let deletePosition = (index) => {
    positions.splice(index, 1);
}

module.exports= {
    getPositions,
    addPosition,
    deletePosition
};