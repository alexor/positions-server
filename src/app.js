const express = require('express')
const app = express()
const geo = require('./geo');


app.use(express.json());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});


app.get('/geo', (req, res) => {
    console.log('get');
    res.send(geo.getPositions());    
});

app.post('/geo', (req, res) => {
    var position = req.body.data;
    geo.addPosition(position);
    console.log('post');
    res.send({success: true});
});

app.delete('/geo/:id', (req, res) => {
    //var data = req.body.data;
    geo.deletePosition(req.params.id);
    console.log('delete');
    res.send({success: true});
});



app.listen(process.env.PORT || 5000, () => {

    console.log('Application is Listening on port: ' + (process.env.PORT || 5000));
   
});
